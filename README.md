The Docker image and Docker-compose implementation. 

Step 1-: Clone the repository  
$ git clone git@gitlab.com:arvind.laitkor/docker-wp.git

Step 2-:  
$ cd docker-wp

Step 3-:  
$ docker-compose up -d

Note-: To make changes in the docker images open the Dockerfile and save it.  
Create build   

$ docker build -t arvind04/wordpress .

To restart the docker container. You can use docker-compose restart or docker-compose up -d  

$ docker-compose up -d 

The above command will refresh the running container.  