FROM ubuntu:latest
MAINTAINER Arvind Rawat (arvind.rawat@laitkor.com)

# install dependencies
RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
		apache2 \
		ca-certificates openssl \
		curl \
		php7.0 \
		php7.0-curl \
		php7.0-gd \
		php7.0-json \
		php7.0-simplexml \
		php7.0-mbstring \
		php7.0-mysql \
		php7.0-zip \
		libapache2-mod-php7.0 \
	&& rm -r /var/lib/apt/lists/*
RUN sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf
RUN a2enmod rewrite expires
RUN a2enmod ssl
RUN a2enmod headers

## Mount ##
#WORKDIR /var/www/html
##Wordpress ##

ENV WORDPRESS_VERSION 4.6.1
ENV WORDPRESS_SHA1 027e065d30a64720624a7404a1820e6c6fff1202

RUN set -x \
	&& curl -o wordpress.tar.gz -fSL "https://wordpress.org/wordpress-${WORDPRESS_VERSION}.tar.gz" \
	&& echo "$WORDPRESS_SHA1 *wordpress.tar.gz" | sha1sum -c - \
# upstream tarballs include ./wordpress/ so this gives us /usr/src/wordpress
	&& tar -xzf wordpress.tar.gz -C /var/www/html \
	&& rm wordpress.tar.gz \
	&& chown -R www-data:www-data /var/www/html/wordpress

#VOLUME /var/www/html
# Default command	
CMD ["apachectl", "-D", "FOREGROUND"]

# Ports
EXPOSE 80
